// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:task_week4_mobile/minggu%208/controller/demo_controller.dart';

class DemoPage extends StatelessWidget {
  final DemoController controller = Get.put(DemoController());
  DemoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Demo Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Padding(
              padding: EdgeInsets.all(8),
              child: Text(Get.arguments.toString()),
            ),
            SwitchListTile(
              value: controller.isDark,
              onChanged: controller.changeTheme,
              title: Text('Dark Theme'),
            ),
            ElevatedButton(
              onPressed: () => Get.snackbar(
                'Hello',
                'Ini Sebuah Informasi',
                snackPosition: SnackPosition.BOTTOM,
                duration: Duration(seconds: 2),
                colorText: Colors.white,
                backgroundColor: Colors.blue,
              ),
              child: Text('Snackbar'),
            ),
            ElevatedButton(
              onPressed: () => Get.defaultDialog(
                title: 'Default Dialog',
                content: Text('Ini Sebuah Informasi'),
                actions: [
                  FlatButton(
                    onPressed: () => Get.back(),
                    child: Text('Close'),
                  ),
                ],
              ),
              child: Text('Dialog'),
            ),
            ElevatedButton(
              onPressed: () => Get.bottomSheet(
                Container(
                  height: 200,
                  child: Center(
                    child: Text('Ini Sebuah Informasi'),
                  ),
                ),
              ),
              child: Text('Bottom Sheet'),
            ),
            ElevatedButton(
              onPressed: () => Get.offNamed('/'),
              child: Text('Back'),
            ),
          ],
        ),
      ),
    );
  }
}
