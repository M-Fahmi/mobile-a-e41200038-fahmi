import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(new MaterialApp(
    home: new HomeState(),
  ));
}

class HomeState extends StatefulWidget {
  const HomeState({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeState> {
  @override
  final List<String> gambar = ["1.gif", "2.gif", "3.gif"];

  static const Map<String, Color> colors = {
    '1': Color.fromARGB(245, 14, 14, 150),
    '2': Color.fromARGB(255, 255, 3, 120),
    '3': Color.fromARGB(251, 28, 60, 202),
  };
  Widget build(BuildContext context) {
    timeDilation = 4.0;
    return new Scaffold(
      body: new Container(
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [Colors.black, Colors.blueAccent, Colors.blueGrey]),
          ),
          child: new PageView.builder(
              controller: new PageController(viewportFraction: 0.5),
              itemCount: gambar.length,
              itemBuilder: (BuildContext context, int i) {
                return new Padding(
                  padding:
                      new EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                  child: new Material(
                    elevation: 8.0,
                    child: new Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        new Hero(
                          tag: gambar[i],
                          child: new Material(
                              child: new InkWell(
                            child: new Flexible(
                              flex: 1,
                              child: Container(
                                color: colors.values.elementAt(i),
                                child: new Image.asset(
                                  "img/${gambar[i]}",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            onTap: () => Navigator.of(context).push(
                                new MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        new Halamandua(
                                          gambar: gambar[i],
                                          colors: colors.values.elementAt(i),
                                        ))),
                          )),
                        ),
                      ],
                    ),
                  ),
                );
              })),
    );
  }
}

class Halamandua extends StatelessWidget {
  const Halamandua({Key? key, required this.gambar, required this.colors})
      : super(key: key);
  final String gambar;
  final Color colors;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("BT21"),
        backgroundColor: Colors.blueGrey,
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                gradient: new RadialGradient(
                    center: Alignment.center,
                    colors: [Colors.white, Colors.blueAccent, Colors.black])),
          ),
          new Center(
            child: new Hero(
                tag: gambar,
                child: new ClipOval(
                  child: new SizedBox(
                    width: 200.0,
                    height: 200.0,
                    child: new Material(
                      child: new InkWell(
                        onTap: () => Navigator.of(context).pop(),
                        child: new Flexible(
                          flex: 1,
                          child: Container(
                            color: colors,
                            child: new Image.asset(
                              "img/$gambar",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
}
