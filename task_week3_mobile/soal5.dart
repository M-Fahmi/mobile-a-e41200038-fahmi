// importing dart:io file
import 'dart:io';

void main() {
  print("Apakah anda akan menginstall Dart? Y/T");
  // Reading name
  var install = stdin.readLineSync();

  install == "Y"
      ? print("Terinstall")
      : install == "T"
          ? print("Not Yet")
          : print("Gagal");
}
