import 'oop2.dart';

void main(List<String> args) {
  pers_panjang persegi; //inisialisasi
  double luasPersegi; // inisialisasi tipe data
  persegi = pers_panjang(); //memanggil object persegipanjang2
  persegi.panjang = 3.0; //set nilai
  persegi.lebar = -2.0; //set nilai

  luasPersegi = persegi.luas; //alias
  print(luasPersegi); //cetak
}
