class pers_panjang {
  late double _panjang; //inisialisasi tipe data
  late double _lebar; // inisialisasi tipe data lebar
  void set lebar(double value) {
    if (value < 0) {
      //validasi
      value *= -1; //return -1
    }
    _lebar = value; //alias
  }

  double get lebar {
    //getter lebar
    return _lebar; //mengembalikan lebar
  }

  void set panjang(double value) {
    if (value < 0) {
      //validasi
      value *= -1; // return -1
    }
    _panjang = value; //alias
  }

  double get panjang {
    return _panjang; //return panjang
  }

  double get luas => _panjang * _lebar; //hitung luas
}
