class Mobil {
  void driving() {
    print("Mengendarai mobil Fortuner");
  }
}

class Honda extends Mobil {
  //override method overrides generic driving method
  @override
  void driving() {
    print("Mengendarai mobil Pajero");
    super.driving(); //calls generic driving method
  }
}

void main() {
  Honda car1 = new Honda();
  car1.driving();
}
